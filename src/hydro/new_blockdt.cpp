//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details
//========================================================================================
//! \file new_blockdt.cpp
//  \brief computes timestep using CFL condition on a MEshBlock

// C/C++ headers
#include <algorithm>  // min()
#include <cfloat>     // FLT_MAX
#include <cmath>      // fabs(), sqrt()

// Athena++ headers
#include "hydro.hpp"
#include "../athena.hpp"
#include "../athena_arrays.hpp"
#include "../eos/eos.hpp"
#include "../mesh/mesh.hpp"
#include "../coordinates/coordinates.hpp"
#include "../field/field.hpp"
#include "hydro_diffusion/hydro_diffusion.hpp"
#include "../field/field_diffusion/field_diffusion.hpp"

// MPI/OpenMP header
#ifdef MPI_PARALLEL
#include <mpi.h>
#endif

#ifdef OPENMP_PARALLEL
#include <omp.h>
#endif


//----------------------------------------------------------------------------------------
// \!fn Real Hydro::NewBlockTimeStep(void)
// \brief calculate the minimum timestep within a MeshBlock

Real Hydro::NewBlockTimeStep(void) {
  MeshBlock *pmb=pmy_block;
  int is = pmb->is; int js = pmb->js; int ks = pmb->ks;
  int ie = pmb->ie; int je = pmb->je; int ke = pmb->ke;
  AthenaArray<Real> w_in,bcc_in,b_x1f_in,b_x2f_in,b_x3f_in;
  w_in.InitWithShallowCopy(pmb->phydro->w);
  if (MAGNETIC_FIELDS_ENABLED) {
    bcc_in.InitWithShallowCopy(pmb->pfield->bcc);
    b_x1f_in.InitWithShallowCopy(pmb->pfield->b.x1f);
    b_x2f_in.InitWithShallowCopy(pmb->pfield->b.x2f);
    b_x3f_in.InitWithShallowCopy(pmb->pfield->b.x3f);      
  }

  auto w = w_in.get_KView4D();
  auto bcc = bcc_in.get_KView4D();
  auto b_x1f = b_x1f_in.get_KView3D();
  auto b_x2f = b_x2f_in.get_KView3D();
  auto b_x3f = b_x3f_in.get_KView3D();

  //Get the EOS and RegionSize so the KOKKOS_LAMBDA can grab it
  EquationOfState peos = *(pmb->peos);
  RegionSize block_size = pmb->block_size;

  Real dt_1,dt_2,dt_3;
  if (COORDINATE_SYSTEM == "cartesian" && !GENERAL_RELATIVITY) {
    dt_1 = (block_size.x1max - block_size.x1min)/(block_size.nx1);
    dt_2 = (block_size.x2max - block_size.x2min)/(block_size.nx2);
    dt_3 = (block_size.x3max - block_size.x3min)/(block_size.nx3);
  } else {
    throw std::runtime_error("new_blockdt unsupported for non-cartesian and GR");
  }

  //Create a min reducer for the Kokkos parallel reduce
  Real min_dt = (FLT_MAX);
  Kokkos::Min< Real > reducer_min(min_dt);

  Kokkos::parallel_reduce("NewBlockTimeStep",
    Kokkos::MDRangePolicy< Kokkos::Rank<3> >({ks,js,is},{ke+1,je+1,ie+1},{1,1,ie+1-is}),
    KOKKOS_LAMBDA( const int k, const int j, const int i,Real& mindt){

      Real dt1 = dt_1;
      Real dt2 = dt_2;
      Real dt3 = dt_3;
      Real wi[(NWAVE)];

      if (!RELATIVISTIC_DYNAMICS) {
        wi[IDN]=w(IDN,k,j,i);
        wi[IVX]=w(IVX,k,j,i);
        wi[IVY]=w(IVY,k,j,i);
        wi[IVZ]=w(IVZ,k,j,i);
        if (NON_BAROTROPIC_EOS) wi[IPR]=w(IPR,k,j,i);

        if (MAGNETIC_FIELDS_ENABLED) {

          Real bx = bcc(IB1,k,j,i) + fabs(b_x1f(k,j,i)-bcc(IB1,k,j,i));
          wi[IBY] = bcc(IB2,k,j,i);
          wi[IBZ] = bcc(IB3,k,j,i);
          Real cf = peos.FastMagnetosonicSpeed(wi,bx);
          dt1 /= (fabs(wi[IVX]) + cf);

          wi[IBY] = bcc(IB3,k,j,i);
          wi[IBZ] = bcc(IB1,k,j,i);
          bx = bcc(IB2,k,j,i) + fabs(b_x2f(k,j,i)-bcc(IB2,k,j,i));
          cf = peos.FastMagnetosonicSpeed(wi,bx);
          dt2 /= (fabs(wi[IVY]) + cf);

          wi[IBY] = bcc(IB1,k,j,i);
          wi[IBZ] = bcc(IB2,k,j,i);
          bx = bcc(IB3,k,j,i) + fabs(b_x3f(k,j,i)-bcc(IB3,k,j,i));
          cf = peos.FastMagnetosonicSpeed(wi,bx);
          dt3 /= (fabs(wi[IVZ]) + cf);

        } else {

          Real cs = peos.SoundSpeed(wi);
          dt1 /= (fabs(wi[IVX]) + cs);
          dt2 /= (fabs(wi[IVY]) + cs);
          dt3 /= (fabs(wi[IVZ]) + cs);

        }
      }

      mindt = fmin(mindt,dt1);

      if (block_size.nx2 > 1) {
        mindt = fmin(mindt,dt2);
      }

      if (block_size.nx3 > 1) {
        mindt = fmin(mindt,dt3);
      }

    //},min_dt);
    },reducer_min);

  // calculate the timestep limited by the diffusion process
  if (phdif->hydro_diffusion_defined) {
    Real mindt_vis, mindt_cnd;
    phdif->NewHydroDiffusionDt(mindt_vis, mindt_cnd);
    min_dt = std::min(min_dt,mindt_vis);
    min_dt = std::min(min_dt,mindt_cnd);
  } // hydro diffusion

  if(MAGNETIC_FIELDS_ENABLED &&
     pmb->pfield->pfdif->field_diffusion_defined) {
    Real mindt_oa, mindt_h;
    pmb->pfield->pfdif->NewFieldDiffusionDt(mindt_oa, mindt_h);
    min_dt = std::min(min_dt,mindt_oa);
    min_dt = std::min(min_dt,mindt_h);
  } // field diffusion

  min_dt *= pmb->pmy_mesh->cfl_number;

  if (UserTimeStep_!=NULL) {
    min_dt = std::min(min_dt, UserTimeStep_(pmb));
  }

  pmb->new_block_dt=min_dt;
  return min_dt;
}
