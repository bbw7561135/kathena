//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details
//
// Edited by Katie Schram 2018
// Edited by Forrrest Glines 2019
//========================================================================================
//! \file taylor_green.cpp
//  \brief Problem generator for Taylor-Green vortex problem.
//
// REFERENCE: Brachet, M. E., Bustamante, M. D., Krstulovic, G., et al. 2013,
// Physical Review E, 87 Ideal evolution of MHD turbulence when imposing
// Taylor-Green symmetries
//========================================================================================

// C++ headers
#include <iostream>   // endl
#include <sstream>    // stringstream
#include <stdexcept>  // runtime_error
#include <string>     // c_str()
#include <algorithm>  // min, max
#include <cmath>

// Athena++ headers
#include "../globals.hpp"
#include "../athena.hpp"
#include "../athena_arrays.hpp"
#include "../parameter_input.hpp"
#include "../mesh/mesh.hpp"
#include "../hydro/hydro.hpp"
#include "../field/field.hpp"
#include "../eos/eos.hpp"
#include "../coordinates/coordinates.hpp"

#ifdef MPI_PARALLEL
#include <mpi.h>
#endif



//========================================================================================
//! \fn void MeshBlock::ProblemGenerator(ParameterInput *pin)
//  \brief Linear wave problem generator for 1D/2D/3D problems.
//========================================================================================

void MeshBlock::ProblemGenerator(ParameterInput *pin) {

  //Get user defined variables
  Real M0 = pin->GetReal("problem","M0");
  Real rho0 = pin->GetReal("problem","rho0");
  Real p0 = pin->GetReal("problem","p0");
  Real L = (pin->GetReal("mesh","x1max")-pin->GetReal("mesh","x1min"))/(2*PI);
  Real gamma = pin->GetReal("hydro","gamma");

  //Check that LX == LY == LZ
  if ((pin->GetReal("mesh","x1max")-pin->GetReal("mesh","x1min")) != 
        (pin->GetReal("mesh","x2max")-pin->GetReal("mesh","x2min")) ||
      (pin->GetReal("mesh","x1max")-pin->GetReal("mesh","x1min")) != 
        (pin->GetReal("mesh","x3max")-pin->GetReal("mesh","x3min"))) {
    std::stringstream msg;
    msg << "### FATAL ERROR in taylor_green.cpp ProblemGenerator" << std::endl
        << "Talyor Green vortex requires cubic domain" << std::endl;
    throw std::runtime_error(msg.str().c_str());
  }

  //Check if parameters give negative pressures
  if ( M0 > std::sqrt( 16. / (6. * gamma) ) ) {
    std::stringstream msg;
    msg << "### FATAL ERROR in taylor_green.cpp ProblemGenerator" << std::endl
        << "Mach number" << M0 <<" and adiabatic index "<< gamma 
        << " leads to negative pressures" << std::endl;
    throw std::runtime_error(msg.str().c_str());
  }

  Real c = std::sqrt(gamma*p0/rho0);
  Real v0 = c*M0;

  //Make some temporary arrays to do everything here on the host
  //(out-scopes the class members)
  auto x1v = Kokkos::create_mirror_view(pcoord->x1v.get_KView1D());
  auto x2v = Kokkos::create_mirror_view(pcoord->x2v.get_KView1D());
  auto x3v = Kokkos::create_mirror_view(pcoord->x3v.get_KView1D());
  Kokkos::deep_copy(x1v,pcoord->x1v.get_KView1D());
  Kokkos::deep_copy(x2v,pcoord->x2v.get_KView1D());
  Kokkos::deep_copy(x3v,pcoord->x3v.get_KView1D());


  auto u = Kokkos::create_mirror_view(phydro->u.get_KView4D());


  // initialize conserved variables
  for (int k=ks; k<=ke; k++) {
  for (int j=js; j<=je; j++) {
  for (int i=is; i<=ie; i++) {

    //Pressure
    Real p = p0 + (rho0*v0*v0/16)*(cos(2*x1v(i)/L)+cos(2*x2v(j)/L))*(cos(2*x3v(k)/L)+2);
    Real rho = p*rho0/p0;

    u(IDN,k,j,i) = rho; //density
    u(IM1,k,j,i) =  rho*v0*sin(x1v(i)/L)*cos(x2v(j)/L)*cos(x3v(k)/L); //x momentum
    u(IM2,k,j,i) = -rho*v0*cos(x1v(i)/L)*sin(x2v(j)/L)*cos(x3v(k)/L); //y momentum
    u(IM3,k,j,i) = 0.0; //z momentum

    if (NON_BAROTROPIC_EOS) {
        u(IEN,k,j,i) = 0.5*rho*
        ((u(IM1,k,j,i)/rho)*(u(IM1,k,j,i)/rho) +
         (u(IM2,k,j,i)/rho)*(u(IM2,k,j,i)/rho) +
         (u(IM3,k,j,i)/rho)*(u(IM3,k,j,i)/rho)) +
        rho*p/(rho*(gamma-1.0));
    }

  }}}

  
  //Initialize the magnetic fields
  if (MAGNETIC_FIELDS_ENABLED) {
    int nx1 = (ie-is)+1 + 2*(NGHOST);
    int nx2 = (je-js)+1 + 2*(NGHOST);
    int nx3 = (ke-ks)+1 + 2*(NGHOST);

    Kokkos::View<Real*** , Kokkos::LayoutRight, HostSpace> ax("ax",nx3,nx2,nx1);
    Kokkos::View<Real*** , Kokkos::LayoutRight, HostSpace> ay("ay",nx3,nx2,nx1);
    Kokkos::View<Real*** , Kokkos::LayoutRight, HostSpace> az("az",nx3,nx2,nx1);

    // initial magnetic field configuration
    int b_config = pin->GetInteger("problem","b_config");

    // initial magnetic field strength (in units of 1/sqrt(4pi))
    Real b0 = pin->GetReal("problem","b0");

    auto b_x1f = Kokkos::create_mirror_view(pfield->b.x1f.get_KView3D());
    auto b_x2f = Kokkos::create_mirror_view(pfield->b.x2f.get_KView3D());
    auto b_x3f = Kokkos::create_mirror_view(pfield->b.x3f.get_KView3D());

    auto x1f = Kokkos::create_mirror_view(pcoord->x1f.get_KView1D());
    auto x2f = Kokkos::create_mirror_view(pcoord->x2f.get_KView1D());
    auto x3f = Kokkos::create_mirror_view(pcoord->x3f.get_KView1D());
    Kokkos::deep_copy(x1f,pcoord->x1f.get_KView1D());
    Kokkos::deep_copy(x2f,pcoord->x2f.get_KView1D());
    Kokkos::deep_copy(x3f,pcoord->x3f.get_KView1D());

    auto dx1f = Kokkos::create_mirror_view(pcoord->dx1f.get_KView1D());
    auto dx2f = Kokkos::create_mirror_view(pcoord->dx2f.get_KView1D());
    auto dx3f = Kokkos::create_mirror_view(pcoord->dx3f.get_KView1D());
    Kokkos::deep_copy(dx1f,pcoord->dx1f.get_KView1D());
    Kokkos::deep_copy(dx2f,pcoord->dx2f.get_KView1D());
    Kokkos::deep_copy(dx3f,pcoord->dx3f.get_KView1D());


    if( b_config == 0) {
      //Uniform magnetic field
      for (int k=ks; k<=ke; k++) {
      for (int j=js; j<=je; j++) {
      for (int i=is; i<=ie+1; i++) {
        b_x1f(k,j,i) = b0;
      }}}
    } else if (b_config == 1) {
      // (b_config=1): insulating conditions
      
      // initial vector potential
      for (int k=ks; k<=ke+1; k++) {
      for (int j=js; j<=je+1; j++) {
      for (int i=is; i<=ie+1; i++) {
          ax(k,j,i) = -b0*sin(x1f(i)/L)*cos(x2f(j)/L)*cos(x3f(k)/L);
          ay(k,j,i) =  b0*cos(x1f(i)/L)*sin(x2f(j)/L)*cos(x3f(k)/L);
      }}}

    } else {
      std::stringstream msg;
      msg << "### FATAL ERROR in taylor_green.cpp ProblemGenerator" << std::endl
          << "Only b_config=0 (uniform magnetic field)" 
          << "or b_config=1 (insulating magnetic field boundaries) "
          << "supported" << std::endl;
      throw std::runtime_error(msg.str().c_str());
    }

    // initialize interface B
    for (int k=ks; k<=ke; k++) {
    for (int j=js; j<=je; j++) {
    for (int i=is; i<=ie+1; i++) {
      b_x1f(k,j,i) += (az(k,j+1,i) - az(k,j,i))/dx2f(j) -
                     (ay(k+1,j,i) - ay(k,j,i))/dx3f(k);
    }}}
    for (int k=ks; k<=ke; k++) {
    for (int j=js; j<=je+1; j++) {
    for (int i=is; i<=ie; i++) {
      b_x2f(k,j,i) += (ax(k+1,j,i) - ax(k,j,i))/dx3f(k) -
                     (az(k,j,i+1) - az(k,j,i))/dx1f(i);
    }}}
    for (int k=ks; k<=ke+1; k++) {
    for (int j=js; j<=je; j++) {
    for (int i=is; i<=ie; i++) {
      b_x3f(k,j,i) += (ay(k,j,i+1) - ay(k,j,i))/dx1f(i) -
                     (ax(k,j+1,i) - ax(k,j,i))/dx2f(j);
    }}}


    // initialize total energy
    if (NON_BAROTROPIC_EOS) {
      for (int k=ks; k<=ke; k++) {
      for (int j=js; j<=je; j++) {
        for (int i=is; i<=ie; i++) {
          u(IEN,k,j,i) +=
            0.5*(SQR(0.5*(b_x1f(k,j,i) + b_x1f(k,j,i+1))) +
                 SQR(0.5*(b_x2f(k,j,i) + b_x2f(k,j+1,i))) +
                 SQR(0.5*(b_x3f(k,j,i) + b_x3f(k+1,j,i))));
        }
      }}
    }

    //Copy b back to device memory
    Kokkos::deep_copy(pfield->b.x1f.get_KView3D(),b_x1f);
    Kokkos::deep_copy(pfield->b.x2f.get_KView3D(),b_x2f);
    Kokkos::deep_copy(pfield->b.x3f.get_KView3D(),b_x3f);
  }

  //Copy u back to device memory
  Kokkos::deep_copy(phydro->u.get_KView4D(),u);
  return;
}

