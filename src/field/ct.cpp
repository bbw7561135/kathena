//========================================================================================
// Athena++ astrophysical MHD code
// Copyright(C) 2014 James M. Stone <jmstone@princeton.edu> and other code contributors
// Licensed under the 3-clause BSD License, see LICENSE file for details
//========================================================================================
//! \file ct.cpp
//  \brief

// C++ headers
#include <algorithm>  // max(), min()

// Athena++ headers
#include "field.hpp"
#include "../athena.hpp"
#include "../athena_arrays.hpp"
#include "../mesh/mesh.hpp"
#include "../coordinates/coordinates.hpp"
#include "../bvals/bvals.hpp"

// OpenMP header
#ifdef OPENMP_PARALLEL
#include <omp.h>
#endif

//----------------------------------------------------------------------------------------
//! \fn  void Field::CT
//  \brief Constrained Transport implementation of dB/dt = -Curl(E), where E=-(v X B)

void Field::CT(const Real wght, FaceField &b_out) {
  MeshBlock *pmb=pmy_block;
  int is = pmb->is; int js = pmb->js; int ks = pmb->ks;
  int ie = pmb->ie; int je = pmb->je; int ke = pmb->ke;
  if (pmb->pbval->block_bcs[INNER_X2] == POLAR_BNDRY
    || pmb->pbval->block_bcs[INNER_X2] == POLAR_BNDRY_WEDGE)
    throw std::runtime_error(
      "Logic for polar coordinates has been removed from Field::CT. "
      "Check original implementation to reintroduce.");

  auto e1 = pmb->pfield->e.x1e.get_KView3D();
  auto e2 = pmb->pfield->e.x2e.get_KView3D();
  auto e3 = pmb->pfield->e.x3e.get_KView3D();

  auto b_out_x1f = b_out.x1f.get_KView3D();
  auto b_out_x2f = b_out.x2f.get_KView3D();
  auto b_out_x3f = b_out.x3f.get_KView3D();

  auto dx1f = pmb->pcoord->dx1f.get_KView1D();
  auto dx2f = pmb->pcoord->dx2f.get_KView1D();
  auto dx3f = pmb->pcoord->dx3f.get_KView1D();
  
  Real wghtdt = wght * pmb->pmy_mesh->dt;
  Real wght2D = (pmb->block_size.nx2 > 1 ) ? 1.0 : 0.0;
  Real wght3D = (pmb->block_size.nx3 > 1 ) ? 1.0 : 0.0;

//---- update B1 2D
  if (pmb->block_size.nx2 > 1) {
    athena_for("CT B1",ks,ke,js,je,is,ie+1,
    KOKKOS_LAMBDA (int k, int j, int i) {
      //pmb->pcoord->Face1Area(k,j,is,ie+1,area);
      Real area_i = dx2f(j)*dx3f(k);
      //pmb->pcoord->Edge3Length(k,j  ,is,ie+1,len);
      //pmb->pcoord->Edge3Length(k,j+1,is,ie+1,len_p1);
      Real e3len_i = dx3f(k); // no j dep. for cart. coords.
      
      // pmb->pcoord->Edge2Length(k  ,j,is,ie+1,len);
      // pmb->pcoord->Edge2Length(k+1,j,is,ie+1,len_p1);
      Real e2len_i = dx2f(j);  // no k dep. for cart. coords.

      // add curl(E) in 2D and 3D problem
      b_out_x1f(k,j,i) += wghtdt / area_i * (
        e2len_i * wght3D * (e2(k+1,j,i) - e2(k,j,i))
        - e3len_i * (e3(k,j+1,i) - e3(k,j,i)));

    });
  }

//---- update B2 (curl terms in 1D and 3D problems)
  {
  athena_for("CT B2",ks,ke,js,je+1,is,ie,
  KOKKOS_LAMBDA (int k, int j, int i) {
    // pmb->pcoord->Face2Area(k,j,is,ie,area);
    Real area_i = dx1f(i)*dx3f(k);
    // pmb->pcoord->Edge3Length(k,j,is,ie+1,len);
    // is equal for len(i) and len(i+1)
    Real e3len_i = dx3f(k);
    
    // pmb->pcoord->Edge1Length(k  ,j,is,ie,len);
    // pmb->pcoord->Edge1Length(k+1,j,is,ie,len_p1);
    Real e1len_i = dx1f(i);
    
    b_out_x2f(k,j,i) += wghtdt / area_i * (
      e3len_i * (e3(k,j,i+1) - e3(k,j,i))
      - e1len_i * wght3D * (e1(k+1,j,i) - e1(k,j,i)));
  
  });
  }

//---- update B3 (curl terms in 1D and 2D problems)
  {
  athena_for("CT B3",ks,ke+1,js,je,is,ie,
  KOKKOS_LAMBDA (int k, int j, int i) {
    // pmb->pcoord->Face3Area(k,j,is,ie,area);
    Real area_i = dx1f(i)*dx2f(j);
    // pmb->pcoord->Edge2Length(k,j,is,ie+1,len);
    Real e2len_i = dx2f(j);
      
    // pmb->pcoord->Edge1Length(k,j  ,is,ie,len);
    // pmb->pcoord->Edge1Length(k,j+1,is,ie,len_p1);
    Real e1len_i = dx1f(i);
        
    b_out_x3f(k,j,i) += wghtdt / area_i * (
      e1len_i * wght2D * (e1(k,j+1,i) - e1(k,j,i))
      - e2len_i * (e2(k,j,i+1) - e2(k,j,i)));
  
  });
  }

  return;
}

//----------------------------------------------------------------------------------------
//! \fn  void Field::WeightedAveB
//  \brief Compute weighted average of face-averaged B in time integrator step

void Field::WeightedAveB(FaceField &b_out, FaceField &b_in1, FaceField &b_in2,
                         const Real wght[3]) {
  MeshBlock *pmb=pmy_block;

  if (pmb->pbval->block_bcs[INNER_X2] == POLAR_BNDRY
    || pmb->pbval->block_bcs[INNER_X2] == POLAR_BNDRY_WEDGE)
    throw std::runtime_error(
      "Logic for polar coordinates has been removed from Field::WeightedAveB. "
      "Check original implementation to reintroduce.");

  int is = pmb->is; int js = pmb->js; int ks = pmb->ks;
  int ie = pmb->ie; int je = pmb->je; int ke = pmb->ke;

  auto b_out_x1f = b_out.x1f.get_KView3D();
  auto b_out_x2f = b_out.x2f.get_KView3D();
  auto b_out_x3f = b_out.x3f.get_KView3D();

  auto b_in1_x1f = b_in1.x1f.get_KView3D();
  auto b_in1_x2f = b_in1.x2f.get_KView3D();
  auto b_in1_x3f = b_in1.x3f.get_KView3D();

  auto b_in2_x1f = b_in2.x1f.get_KView3D();
  auto b_in2_x2f = b_in2.x2f.get_KView3D();
  auto b_in2_x3f = b_in2.x3f.get_KView3D();

  const Real wght0 = wght[0];
  const Real wght1 = wght[1];
  const Real wght2 = wght[2];

  // Note: these loops can be combined now that they avoid curl terms
  // Only need to separately account for the final longitudinal face in each loop limit

  // b_in2 may be an unallocated AthenaArray if using a 2S time integrator
  if (wght[2] != 0.0) {
//---- B1
    {
    athena_for("WeightedAveB wght[2] != 0 B1",ks,ke,js,je,is,ie+1,
    KOKKOS_LAMBDA (int k, int j, int i) {
          b_out_x1f(k,j,i) = wght0*b_out_x1f(k,j,i) + wght1*b_in1_x1f(k,j,i)
              + wght2*b_in2_x1f(k,j,i);
    });
    }

//---- B2
    {
    athena_for("WeightedAveB wght[2] != 0 B2",ks,ke,js,je+1,is,ie,
    KOKKOS_LAMBDA (int k, int j, int i) {
          b_out_x2f(k,j,i) = wght0*b_out_x2f(k,j,i) + wght1*b_in1_x2f(k,j,i)
              + wght2*b_in2_x2f(k,j,i);
    });
    }

//---- B3
    {
    athena_for("WeightedAveB wght[2] != 0 B3",ks,ke+1,js,je,is,ie,
    KOKKOS_LAMBDA (int k, int j, int i) {
          b_out_x3f(k,j,i) = wght0*b_out_x3f(k,j,i) + wght1*b_in1_x3f(k,j,i)
              + wght2*b_in2_x3f(k,j,i);
    });
    }
  } else { // do not derefernce b_in2
//---- B1
    {
    athena_for("WeightedAveB wght[2] == 0 B1",ks,ke,js,je,is,ie+1,
    KOKKOS_LAMBDA (int k, int j, int i) {
          b_out_x1f(k,j,i) = wght0*b_out_x1f(k,j,i) + wght1*b_in1_x1f(k,j,i);
    });
    }

//---- B2
    {
    athena_for("WeightedAveB wght[2] == 0 B2",ks,ke,js,je+1,is,ie,
    KOKKOS_LAMBDA (int k, int j, int i) {
          b_out_x2f(k,j,i) = wght0*b_out_x2f(k,j,i) + wght1*b_in1_x2f(k,j,i);
    });
    }

//---- B3
    {
    athena_for("WeightedAveB wght[2] == 0 B3",ks,ke+1,js,je,is,ie,
    KOKKOS_LAMBDA (int k, int j, int i) {
          b_out_x3f(k,j,i) = wght0*b_out_x3f(k,j,i) + wght1*b_in1_x3f(k,j,i);
    });
    }
  }
  return;
}
