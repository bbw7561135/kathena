# Test script for Kokkos partition manager

# Modules
import numpy as np
import sys
import scripts.utils.athena as athena
sys.path.insert(0, '../../vis/python')
import athena_read # noqa


# Prepare Athena++
def prepare(**kwargs):
    athena.configure('b',
                     prob='blast',
                     coord='cartesian',
                     flux='hlld', **kwargs)
    athena.make()


# Run Athena++
def run(**kwargs):
    arguments = [
      'job/problem_id=mhd.hlld.adiabatic.Blast',
      'time/tlim=0.6',
      'mesh/num_threads=4',
      'meshblock/nx2=32',
      'meshblock/nx3=32']
    athena.run('mhd/athinput.blast', arguments)


# Analyze outputs
def analyze():
    headers = ['rho', 'press', 'vel', 'Bcc']
    _, _, _, data_ref = athena_read.vtk(
        'data/mhd.hlld.adiabatic.Blast.block0.out1.00006.vtk')
    _, _, _, data_new0 = athena_read.vtk(
        'bin/mhd.hlld.adiabatic.Blast.block0.out1.00006.vtk')
    _, _, _, data_new1 = athena_read.vtk(
        'bin/mhd.hlld.adiabatic.Blast.block1.out1.00006.vtk')
    _, _, _, data_new2 = athena_read.vtk(
        'bin/mhd.hlld.adiabatic.Blast.block2.out1.00006.vtk')
    _, _, _, data_new3 = athena_read.vtk(
        'bin/mhd.hlld.adiabatic.Blast.block3.out1.00006.vtk')

    for header in headers:
        if header == 'rho' or header == 'press':
            n_fields = 1
        else:
            n_fields = 3
        data_new = np.zeros((64, 64, 64, n_fields), dtype=data_new0[header].dtype)
        data_new[:32, :32, :, :] = data_new0[header].reshape((32, 32, 64, n_fields))
        data_new[:32, 32:, :, :] = data_new1[header].reshape((32, 32, 64, n_fields))
        data_new[32:, :32, :, :] = data_new2[header].reshape((32, 32, 64, n_fields))
        data_new[32:, 32:, :, :] = data_new3[header].reshape((32, 32, 64, n_fields))
        try:
            # vtk data is single precision only, thus decimal 7 should suffice
            np.testing.assert_almost_equal(
                data_new, data_ref[header].reshape((64, 64, 64, n_fields)), decimal=7)
        except AssertionError:
            print('!!! ERROR: ' + header + ' arrays are not identical.')
            return False

    return True
